<?php
namespace ShopExpress\ApiClient;

use InvalidArgumentException;
use ShopExpress\ApiClient\Exception\ApiException;
use ShopExpress\ApiClient\Exception\NetworkException;
use ShopExpress\ApiClient\Exception\RequestException;
use ShopExpress\ApiClient\Request\RequestBuilder;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use ShopExpress\RequestResponse\Request\Request;
use ShopExpress\ApiClient\Response\ApiResponse;
use ShopExpress\ApiClient\Response\ResponseBuilder;

/**
 * Class ApiClient
 */
class ApiClient extends Client
{
    public const ERROR_API_CALLING = 'You have to specify a method (eg. POST, PUT, ...) and a correct object url to call the API';
    public const ERROR_API_MESSAGE = 'Message from API: %s';
    public const ERROR_ARGUMENT_VALUE = 'Parameter `%s` can\'t be empty';

    /**
     * @var string
     */
    protected $apiToken;
    /**
     * @var string
     */
    protected $apiUrl;

    /**
     * ApiClient constructor.
     *
     * @param string $apiUrl The api url
     * @param string $apiToken The api key
     */
    public function __construct(string $apiUrl, string $apiToken)
    {
        $this->setApiUrl($apiUrl);
        $this->setApiToken($apiToken);

        $options = [
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_USERAGENT => 'dicms-api-php-beta-3.0',
        ];

        parent::__construct($options);
    }

    /**
     * Sets the api key.
     *
     * @param string $apiKey The api key
     *
     * @throws InvalidArgumentException
     * @return ApiClient
     */
    protected function setApiToken(string $apiKey): ApiClient
    {
        if (empty($apiKey)) {
            throw new InvalidArgumentException(
                sprintf(self::ERROR_ARGUMENT_VALUE, 'apiKey')
            );
        }
        $this->apiToken = $apiKey;

        return $this;
    }

    /**
     * Sets the api url.
     *
     * @param string $apiUrl The api url
     *
     * @throws InvalidArgumentException
     * @return ApiClient
     */
    public function setApiUrl(string $apiUrl): ApiClient
    {
        if (empty($apiUrl)) {
            throw new InvalidArgumentException(
                sprintf(self::ERROR_ARGUMENT_VALUE, 'apiUrl')
            );
        }
        if (!filter_var($apiUrl, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException(sprintf('Parameter `apiUrl` must be valid URL: %s', $apiUrl));
        }
        if ('/' !== $apiUrl[strlen($apiUrl) - 1]) {
            $apiUrl .= '/';
        }
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * Gets the api key.
     *
     * @return string The api key.
     */
    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    /**
     * Gets the api url.
     *
     * @return string The api url.
     */
    public function getApiUrl(): string
    {
        return $this->apiUrl;
    }

    /**
     * Makes a request.
     *
     * @param Request $request
     *
     * @throws NetworkException
     * @throws RequestException
     * @return ApiResponse
     */
    protected function makeRequest(Request $request): ApiResponse
    {
        /** @var ApiResponse $response */
        $response = $this->sendRequest($request);
        return $this->validateResponse($response->parseBody());
    }

    /**
     * Validate response.
     *
     * @param ApiResponse $response The response
     *
     * @throws ApiException
     *
     * @return ApiResponse
     */
    protected function validateResponse(ApiResponse $response): ApiResponse
    {
        if (!empty($response['result']['message'])) {
            throw new ApiException(
                sprintf(self::ERROR_API_MESSAGE, $response['result']['message'])
            );
        }

        return $response;
    }

    /**
     * Universal api request method.
     *
     * @param string $method The method
     * @param RequestBuilder $requestBuilder
     *
     * @throws Exception\InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     * @return ApiResponse
     */
    public function api(string $method, RequestBuilder $requestBuilder): ApiResponse
    {
        if (!empty($method)) {
            return $this->makeRequest($requestBuilder->build($method));
        }

        throw new InvalidArgumentException(self::ERROR_API_CALLING);
    }

    /**
     * Get api method.
     *
     * @param RequestBuilder $requestBuilder
     *
     * @throws Exception\InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     * @return ApiResponse
     */
    public function get(RequestBuilder $requestBuilder): ApiResponse
    {
        return $this->makeRequest($requestBuilder->build(RequestBuilder::REQUEST_TYPE_GET));
    }

    /**
     * Update api method.
     *
     * @param RequestBuilder $requestBuilder
     *
     * @throws Exception\InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     * @return ApiResponse
     */
    public function update(RequestBuilder $requestBuilder): ApiResponse
    {
        return $this->makeRequest($requestBuilder->build(RequestBuilder::REQUEST_TYPE_PUT));
    }

    /**
     * Create api method.
     *
     * @param RequestBuilder $requestBuilder
     *
     * @throws Exception\InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     * @return ApiResponse
     */
    public function create(RequestBuilder $requestBuilder): ApiResponse
    {
        return $this->makeRequest($requestBuilder->build(RequestBuilder::REQUEST_TYPE_POST));
    }

    /**
     * Delete api method.
     *
     * @param RequestBuilder $requestBuilder
     *
     * @throws Exception\InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     * @return ApiResponse
     */
    public function delete(RequestBuilder $requestBuilder): ApiResponse
    {
        return $this->makeRequest($requestBuilder->build(RequestBuilder::REQUEST_TYPE_DELETE));
    }

    /**
     * @return ResponseBuilder
     */
    protected function createResponseBuilder(): ResponseBuilder
    {
        $response = new ApiResponse();
        return new ResponseBuilder($response);
    }

    /**
     * @param string $serviceUrl
     * @param null $id
     * @param array $params
     *
     * @return string
     */
    protected function retrieveServiceUrl(string $serviceUrl, $id = null, array $params = []): string
    {
        $serviceUrl = parent::retrieveServiceUrl($serviceUrl, $id, $params + ['token' => $this->getApiToken()]);
        return rtrim($this->apiUrl, '/') . '/' . $serviceUrl;
    }
}
