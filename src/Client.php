<?php

namespace ShopExpress\ApiClient;

use ShopExpress\ApiClient\Response\Response;
use ShopExpress\RequestResponse\Request\Request;
use ShopExpress\ApiClient\Response\ResponseBuilder;
use ShopExpress\ApiClient\Exception\NetworkException;
use ShopExpress\ApiClient\Exception\RequestException;

/**
 * Class Client
 * Wrapper class for cURL
 */
class Client
{
    /**
     * cURL options.
     *
     * @var array
     */
    private $curlOptions;

    /**
     * cURL synchronous requests handle.
     *
     * @var resource|null
     */
    private $handle;

    /**
     * Create HTTP client.
     *
     * @param array $options cURL options
     */
    public function __construct(array $options = [])
    {
        $defaults = [
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
        ];

        $this->curlOptions = array_replace($defaults, $options);
    }

    public function __destruct()
    {
        if (is_resource($this->handle)) {
            curl_close($this->handle);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws NetworkException
     * @throws RequestException
     */
    public function sendRequest(Request $request): Response
    {
        $responseBuilder = $this->createResponseBuilder();
        $requestOptions = $this->prepareRequestOptions($request, $responseBuilder);

        if (is_resource($this->handle)) {
            curl_reset($this->handle);
        } else {
            $this->handle = curl_init();
        }

        curl_setopt_array($this->handle, $requestOptions);
        curl_exec($this->handle);

        $errno = curl_errno($this->handle);

        switch ($errno) {
            case CURLE_OK:
                // All OK, no actions needed.
                break;
            case CURLE_COULDNT_RESOLVE_PROXY:
            case CURLE_COULDNT_RESOLVE_HOST:
            case CURLE_COULDNT_CONNECT:
            case CURLE_OPERATION_TIMEOUTED:
            case CURLE_SSL_CONNECT_ERROR:
                throw new NetworkException(
                    sprintf('Network error to url `%s`: %s', curl_getinfo($this->handle, CURLINFO_EFFECTIVE_URL), curl_error($this->handle))
                );
            default:
                throw new RequestException(
                    sprintf('Request to url `%s` failed: %s', curl_getinfo($this->handle, CURLINFO_EFFECTIVE_URL), curl_error($this->handle))
                );
        }

        return $responseBuilder->getResponse();
    }

    /**
     * @return ResponseBuilder
     */
    protected function createResponseBuilder(): ResponseBuilder
    {
        return new ResponseBuilder(new Response());
    }

    /**
     * @param Request $request Request on which to create options.
     * @param ResponseBuilder $responseBuilder Builder to use for building response.
     *
     * @return array cURL options based on request.
     */
    private function prepareRequestOptions(
        Request $request,
        ResponseBuilder $responseBuilder
    ): array {
        $curlOptions = $this->curlOptions;

        $curlOptions[CURLOPT_URL] = $this->retrieveServiceUrl($request->getStartUrl(), $request->getId(), $request->get());

        $curlOptions = $this->addRequestBodyOptions($request, $curlOptions);

        $curlOptions[CURLOPT_HTTPHEADER] = $this->createHeaders($request, $curlOptions);

        $curlOptions[CURLOPT_HEADERFUNCTION] = static function ($ch, $data) use ($responseBuilder) {
            $str = trim($data);
            if ('' !== $str) {
                if (stripos($str, 'http/') === 0) {
                    $responseBuilder->setStatus($str);
                } else {
                    $responseBuilder->addHeader($str);
                }
            }

            return strlen($data);
        };

        $curlOptions[CURLOPT_WRITEFUNCTION] = static function ($ch, $data) use ($responseBuilder) {
            return $responseBuilder->getResponse()->appendBody($data);
        };

        return $curlOptions;
    }

    /**
     * Add request body related cURL options.
     *
     * @param Request $request Request on which to create options.
     * @param array $curlOptions Options created by prepareRequestOptions().
     *
     * @return array cURL options based on request.
     */
    private function addRequestBodyOptions(Request $request, array $curlOptions): array
    {
        /*
         * Some HTTP methods cannot have payload:
         *
         * - GET — cURL will automatically change method to PUT or POST if we set CURLOPT_UPLOAD or
         *   CURLOPT_POSTFIELDS.
         * - HEAD — cURL treats HEAD as GET request with a same restrictions.
         * - TRACE — According to RFC7231: a client MUST NOT send a message body in a TRACE request.
         */
        if (!in_array($request->getMethod(), ['GET', 'HEAD', 'TRACE'], true)) {
            $body = json_encode($request->get());
            $bodySize = strlen($body);
            if ($bodySize !== 0) {

                // Message has non empty body.
                if ($bodySize > 1024 * 1024) {
                    $body = gzencode($body, 9);
                    $bodySize = strlen($body);
                }

                if ($bodySize > 1024 * 1024) {
                    // Avoid full loading large body into memory
                    $curlOptions[CURLOPT_UPLOAD] = true;
                    $curlOptions[CURLOPT_INFILESIZE] = $bodySize;
                    $curlOptions[CURLOPT_READFUNCTION] = static function ($ch, $fd, $length) use ($body) {
                        static $position = 0;

                        $data = substr($body, $position, $length);
                        $position += strlen($data);

                        return $data;
                    };

                    $request->withHeader('Content-Encoding', 'gzip');
                } else {
                    // Small body can be loaded into memory
                    $curlOptions[CURLOPT_POSTFIELDS] = $body;
                }
            }
        }

        if ($request->getMethod() === 'HEAD') {
            // This will set HTTP method to "HEAD".
            $curlOptions[CURLOPT_NOBODY] = true;
        } elseif ($request->getMethod() !== 'GET') {
            // GET is a default method. Other methods should be specified explicitly.
            $curlOptions[CURLOPT_CUSTOMREQUEST] = $request->getMethod();
        }

        return $curlOptions;
    }

    /**
     * Create headers array for CURLOPT_HTTPHEADER.
     *
     * @param Request $request Request on which to create headers.
     * @param array $curlOptions Options created by prepareRequestOptions().
     *
     * @return string[]
     */
    private function createHeaders(Request $request, array $curlOptions): array
    {
        $curlHeaders = [];
        $headers = $request->getHeaders();
        foreach ($headers as $name => $value) {
            $header = strtolower($name);
            if ('expect' === $header) {
                // curl-client does not support "Expect-Continue", so dropping "expect" headers
                continue;
            }
            if ('content-length' === $header) {
                if (array_key_exists(CURLOPT_POSTFIELDS, $curlOptions)) {
                    // Small body content length can be calculated here.
                    $value = strlen($curlOptions[CURLOPT_POSTFIELDS]);
                } elseif (!array_key_exists(CURLOPT_READFUNCTION, $curlOptions)) {
                    // Else if there is no body, forcing "Content-length" to 0
                    $value = 0;
                }
            }

            $curlHeaders[] = $name . ': ' . $value;
        }
        /*
         * curl-client does not support "Expect-Continue", but cURL adds "Expect" header by default.
         * We can not suppress it, but we can set it to empty.
         */
        $curlHeaders[] = 'Expect:';

        return $curlHeaders;
    }

    /**
     * @param string $serviceUrl The object url
     * @param null $id
     * @param array $params The parameters
     *
     * @return string The init url
     */
    protected function retrieveServiceUrl(string $serviceUrl, $id = null, array $params = []): string
    {
        $queryString = !empty($params) ? '?' . http_build_query($params) : '';
        return rtrim($serviceUrl, '/') . '/' . ($id ?? '') . $queryString;
    }
}
