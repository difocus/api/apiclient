<?php
namespace ShopExpress\ApiClient\Exception;

use RuntimeException;

/**
 * Class ApiException
 * @package ShopExpress\ApiClient\Exception
 */
class ApiException extends RuntimeException
{
}
