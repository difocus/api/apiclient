<?php
namespace ShopExpress\ApiClient\Exception;

use DomainException;

/**
 * Class InvalidJsonException
 * @package ShopExpress\ApiClient\Exception
 */
class InvalidJsonException extends DomainException
{
}
