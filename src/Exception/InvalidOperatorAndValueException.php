<?php

namespace ShopExpress\ApiClient\Exception;

use Exception;

/**
 * Class InvalidOperatorAndValueException
 * @package ShopExpress\ApiClient\Exception
 */
class InvalidOperatorAndValueException extends Exception
{
}