<?php

namespace ShopExpress\ApiClient\Exception;

use Exception;

/**
 * Class NetworkException
 * @package ShopExpress\ApiClient\Exception
 */
class NetworkException extends Exception
{
}
