<?php

namespace ShopExpress\ApiClient\Exception;

use Exception;

/**
 * Class RequestException
 * @package ShopExpress\ApiClient\Exception
 */
class RequestException extends Exception
{
}
