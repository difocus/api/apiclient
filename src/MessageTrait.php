<?php

namespace ShopExpress\ApiClient;

/**
 * Trait MessageTrait
 * Represents common logic shared by Request and Response classes
 */
trait MessageTrait
{
    /** @var array Map of all registered headers, as original name => array of values */
    private $headers = [];

    /** @var array Map of lowercase header name => original name at registration */
    private $headerNames  = [];
    
    /** @var string|null */
    protected $body;

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string $header
     * @return bool
     */
    public function hasHeader(string $header)
    {
        return isset($this->headerNames[strtolower($header)]);
    }

    /**
     * Returns header value by its name. If the header doesn't present, returns empty array
     *
     * @param string $header Name of the header
     * @return array
     */
    public function getHeader(string $header)
    {
        $header = strtolower($header);
    
        if (!isset($this->headerNames[$header])) {
            return [];
        }
    
        $header = $this->headerNames[$header];
    
        return $this->headers[$header];
    }

    public function getBody()
    {
        return $this->body;
    }

    /**
     * Appends string to body
     *
     * @param string $append String to append
     * @return int Appended string length
     */
    public function appendBody(string $append)
    {
        $this->body = $this->body ?? '';

        $this->body .= $append;
        return strlen($append);
    }

    /**
     * @param string|null $body
     */
    public function setBody(?string $body)
    {
        $this->body = $body;
    }

    /**
     * Adds or replaces header value
     *
     * @param string $header
     * @param mixed $value
     * @param bool $add Whether or not to add header
     */
    public function withHeader(string $header, $value, bool $add = false)
    {
        if (!is_array($value)) {
            $value = [$value];
        }
        $value = $this->trimHeaderValues($value);
        $normalized = strtolower($header);
        
        if (isset($this->headerNames[$normalized])) {
            if (!$add) {
                unset($this->headers[$this->headerNames[$normalized]]);
            } else {
                $header = $this->headerNames[$normalized];
                $this->headers[$header] = array_merge($this->headers[$header], $value);
            }
        }
        if (!$add || !isset($this->headerNames[$normalized])) {
            $this->headerNames[$normalized] = $header;
            $this->headers[$header] = $value;
        }
    }

    /**
     * @param array $headers
     */
    private function setHeaders(array $headers)
    {
        $this->headerNames = $this->headers = [];
        foreach ($headers as $header => $value) {
            if (!is_array($value)) {
                $value = [$value];
            }
     
            $value = $this->trimHeaderValues($value);
            $normalized = strtolower($header);
            if (isset($this->headerNames[$normalized])) {
                $header = $this->headerNames[$normalized];
                $this->headers[$header] = array_merge($this->headers[$header], $value);
            } else {
                $this->headerNames[$normalized] = $header;
                $this->headers[$header] = $value;
            }
        }
    }

    /**
     * Trims whitespace from the header values.
     *
     * Spaces and tabs ought to be excluded by parsers when extracting the field value from a header field.
     *
     * header-field = field-name ":" OWS field-value OWS
     * OWS          = *( SP / HTAB )
     *
     * @param string[] $values Header values
     *
     * @return string[] Trimmed header values
     *
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     */
    private function trimHeaderValues(array $values)
    {
        return array_map(function ($value) {
            return trim($value, " \t");
        }, $values);
    }
}
