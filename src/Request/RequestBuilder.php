<?php


namespace ShopExpress\ApiClient\Request;


use InvalidArgumentException;
use ShopExpress\ApiClient\Exception\InvalidOperatorAndValueException;
use ShopExpress\ApiClient\Exception\RequestException;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use ShopExpress\RequestResponse\Request\Request;

/**
 * Class RequestBuilder
 * @package ShopExpress\ApiClient\Request
 */
final class RequestBuilder
{
    public const REQUEST_TYPE_GET = 'GET';
    public const REQUEST_TYPE_PUT = 'PUT';
    public const REQUEST_TYPE_POST = 'POST';
    public const REQUEST_TYPE_DELETE = 'DELETE';

    /**
     * @var string
     */
    private $service;
    /**
     * @var array
     */
    private $content = [];
    /**
     * @var RequestBuilderFilter[]
     */
    private $filters = [];
    /**
     * @var RequestBuilderSort|null
     */
    private $sort;
    /**
     * @var array
     */
    private $pagination = [];
    /**
     * @var array
     */
    private $include = [];

    /**
     * QueryBuilder constructor.
     *
     * Если передается массив в $filters, создает запрос вида /service?filters[key2][]=<>&filters[key2][]='ёжик'
     * Если передается ID, создает запрос вида /service/<ID>
     *
     * @param string $service
     * @param RequestBuilderFilter[]|string|int $filters Массив фильтров либо ID
     * @param RequestBuilderSort|null $sort
     */
    public function __construct(string $service, $filters = [], ?RequestBuilderSort $sort = null)
    {
        $this->service = $service;

        if (is_scalar($filters)) {
            $this->service = rtrim($this->service, '/') . '/' . $filters;
        } else {
            $this->addFilters($filters ?? []);
        }

        if (null !== $sort) {
            $this->sort = $sort;
        }
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return RequestBuilder
     */
    public function paginate(?int $limit = null, ?int $offset = null): RequestBuilder
    {
        if ($limit !== null) {
            $this->pagination['limit'] = $limit;
        }
        if ($offset !== null) {
            $this->pagination['start'] = $offset;
        }

        return $this;
    }

    /**
     * @param array $include
     *
     * @return RequestBuilder
     */
    public function include(array $include): RequestBuilder
    {
        $this->include = $include;
        return $this;
    }

    /**
     * @param string $queryType
     *
     * @throws InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws RequestException
     * @return Request
     */
    public function build(string $queryType): Request
    {
        switch ($queryType) {
            case self::REQUEST_TYPE_PUT:
            case self::REQUEST_TYPE_POST:
            case self::REQUEST_TYPE_DELETE:

                if (null === ($filters = $this->compileFilters())) {
                    $filters = [];
                }

                $params = compact('filters');

                return new Request(
                    $this->getService(), null, $params, [], $this->getData(), [], [], $queryType, [
                        'Content-Type' => 'application/json',
                    ]
                );

            case self::REQUEST_TYPE_GET:

                if (null === ($filters = $this->compileFilters())) {
                    $filters = [];
                }

                if (null === ($sort = $this->compileSort())) {
                    $sort = [];
                }

                $params = array_merge(compact('filters'), compact('sort'), $this->pagination, ['include' => $this->include]);

                return (new Request(
                    $this->getService(), null, $params, [], $this->getData(), [], [], $queryType, [
                        'Content-Type' => 'application/json',
                    ]
                ));

            default:
                throw new RequestException("Can't determine the type of query");
        }
    }

    /**
     * @param RequestBuilderFilter[] $filters
     *
     * @return $this
     */
    public function addFilters(array $filters): self
    {
        foreach ($filters as $key => $filter) {
            $this->addFilter($filter);
        }
        return $this;
    }

    /**
     * @param RequestBuilderFilter $filter
     *
     * @return $this
     */
    public function addFilter(RequestBuilderFilter $filter): self
    {
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * @param RequestBuilderSort|null $sort
     *
     * @return RequestBuilder
     */
    public function setSort(?RequestBuilderSort $sort): RequestBuilder
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * Добавляет SET для update
     *
     * @param array $content
     *
     * @return $this
     */
    public function setData(array $content): RequestBuilder
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->content;
    }

    /**
     * @throws InvalidOperatorAndValueException
     * @return array
     */
    private function compileFilters(): array
    {
        $compiledWheres = [];
        foreach ($this->filters as $filter) {
            $compiledWheres[] = $filter->compile();
        }

        return array_merge_recursive([], ...$compiledWheres);
    }

    /**
     * @return string|null
     */
    private function compileSort(): ?string
    {
        if (null !== $this->sort) {
            return $this->sort->compile();
        }

        return null;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return RequestBuilderSort|null
     */
    public function getSort(): ?RequestBuilderSort
    {
        return $this->sort;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @param string $service
     *
     * @return RequestBuilder
     */
    public function setService(string $service): RequestBuilder
    {
        $this->service = $service;
        return $this;
    }

    public function flush(): void
    {
        $this->service = null;
        $this->sort = null;
        $this->filters = [];
        $this->content = [];
    }
}
