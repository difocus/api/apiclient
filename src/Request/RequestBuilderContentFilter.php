<?php

namespace ShopExpress\ApiClient\Request;


use Iterator;
use ShopExpress\ApiClient\Exception\InvalidOperatorAndValueException;
use ShopExpress\ApiClient\Exception\RequestException;


/**
 * Class RequestBuilderContentFilter
 * @package ShopExpress\ApiClient\Request
 */
class RequestBuilderContentFilter extends RequestBuilderFilter
{
    /**
     * @throws InvalidOperatorAndValueException
     * @return array
     */
    public function compile(): array
    {
        return ['content' => parent::compile()];
    }
}
