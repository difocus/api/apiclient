<?php

namespace ShopExpress\ApiClient\Request;


use Iterator;
use ShopExpress\ApiClient\Exception\InvalidOperatorAndValueException;
use ShopExpress\ApiClient\Exception\RequestException;

/**
 * Class RequestBuilderFilter
 * @package ShopExpress\ApiClient\Request
 */
class RequestBuilderFilter implements Iterator
{
    /**
     * @var array
     */
    private $keys;
    /**
     * @var array
     */
    private $operations;
    /**
     * @var array
     */
    private $values;

    /**
     * All of the available clause operators.
     *
     * @var array
     */
    public $operators = [
        '=', '<', '>', '<=', '>=', '<>', '!=', '<=>',
        'like', 'like binary', 'not like', 'ilike',
        '&', '|', '^', '<<', '>>',
        'rlike', 'regexp', 'not regexp',
        '~', '~*', '!~', '!~*', 'similar to',
        'not similar to', 'not ilike', '~~*', '!~~*',

        'is', 'is not',
    ];

    /**
     * @var array
     */
    public $inOperators = [
        'in', 'not in', 'between',
    ];

    /**
     * QueryBuilderWhere constructor.
     * @param $keys
     * @param $operations
     * @param $values
     * @param string $glue
     * @throws RequestException
     */
    public function __construct($keys, $operations, $values, string $glue = 'AND')
    {
        if (is_array($keys) && is_array($values) && count($keys) !== count($values) && ((is_array($operations) && count($keys) !== count($operations)) || is_string($operations))) {
            throw new RequestException('Неверные аргументы для фильтрации.');
        }

        $wrapArray = !is_array($keys);

        $this->keys = !$wrapArray ? $keys : [$keys];
        $this->operations = $operations;
        $this->values = !$wrapArray ? $values : [$values];
    }

    /**
     * Determine if the given operator and value combination is legal.
     *
     * Prevents using Null values with invalid operators
     * and empty arrays with In, Not In operators.
     *
     * @param string $operator
     * @param mixed $value
     *
     * @return bool
     */
    public function invalidOperatorAndValue($operator, $value): bool
    {
        return $this->invalidSimpleOperatorAndValue($operator, $value)
            || $this->invalidInOperatorAndValue($operator, $value);
    }

    /**
     * @param $operator
     * @param $value
     *
     * @return bool
     */
    protected function invalidSimpleOperatorAndValue($operator, $value): bool
    {
        return (($value === null && !in_array(strtolower($operator), ['is', 'is not']))
                || ($value !== null && in_array(strtolower($operator), ['is', 'is not']))
            ) && !$this->invalidSimpleOperator($operator);
    }

    /**
     * @param $operator
     * @param $value
     *
     * @return bool
     */
    protected function invalidInOperatorAndValue($operator, $value): bool
    {
        return ((is_array($value) && !count($value)) || !is_array($value))
            && !$this->invalidInOperator($operator);
    }

    /**
     * @param $operator
     *
     * @return bool
     */
    protected function invalidSimpleOperator($operator): bool
    {
        return !in_array(strtolower($operator), $this->operators, true);
    }

    /**
     * @param $operator
     *
     * @return bool
     */
    protected function invalidInOperator($operator): bool
    {
        return !in_array(strtolower($operator), $this->inOperators, true);
    }

    /**
     * Determine if the given operator is supported.
     *
     * @param string $operator
     *
     * @return bool
     */
    public function invalidOperator($operator): bool
    {
        return $this->invalidSimpleOperator($operator)
            && $this->invalidInOperator($operator);
    }

    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        $key = key($this->keys);
        return [current($this->keys), is_string($this->operations) ? $this->operations : $this->operations[$key], $this->values[$key]];
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        return next($this->keys);
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return key($this->keys);
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        $key = key($this->keys);
        return ($key !== NULL && $key !== FALSE);

    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        reset($this->keys);
    }

    /**
     * @throws InvalidOperatorAndValueException
     * @return array
     */
    public function compile(): array
    {
        $filters = [];
        foreach ($this as $whereArray) {
            [$where, $operator, $value] = $whereArray;

            if ($this->invalidInOperator($operator)) {
                if (is_array($value) && count($value) === 1) {
                    $value = reset($value);
                }

                if (is_array($value)) {
                    throw new InvalidOperatorAndValueException('Значение для операторов не IN, NOT IN не может быть массивом.');
                }
            } else if (!is_array($value)) {
                $value = [$value];
            }

            if ($this->invalidOperator($operator) || $this->invalidOperatorAndValue($operator, $value)) {
                throw new InvalidOperatorAndValueException('Несовместимое значение оператора и значения.');
            }

            if (!$this->invalidInOperator($operator)) {
                if (strtolower($operator) === 'between') {
                    $filters[$where] = [$operator, $value[0], $value[1]];
                } else {
                    $filters[$where] = [$operator, $value];
                }
            } else if ($value === null) {
                $filters[$where] = [$operator, null];
            } else {
                if (!is_scalar($value)) {
                    $value = (string)$value;
                }
                $filters[$where] = [$operator, $value];
            }
        }

        return $filters;
    }
}
