<?php

namespace ShopExpress\ApiClient\Request;


/**
 * Class RequestBuilderSort
 * @package ShopExpress\ApiClient\Request
 */
class RequestBuilderSort
{
    public const ASC = 'asc';
    public const DESC = 'desc';

    /**
     * @var string
     */
    private $field;
    /**
     * @var string
     */
    private $direction;

    /**
     * QueryBuilderOrder constructor.
     *
     * @param string $field
     * @param string $direction
     */
    public function __construct(string $field, string $direction)
    {
        $this->field = $field;
        $this->direction = $direction;
    }

    /**
     * @param string $direction
     * @return RequestBuilderSort
     */
    public function setDirection(string $direction): RequestBuilderSort
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @param string $field
     *
     * @return RequestBuilderSort
     */
    public function setField(string $field): RequestBuilderSort
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * @return string
     */
    public function compile(): string
    {
        return strtolower($this->getField() . '_' . $this->getDirection());
    }
}
