<?php

namespace ShopExpress\ApiClient\Response;

use ArrayAccess;
use BadMethodCallException;
use InvalidArgumentException;
use ShopExpress\ApiClient\Exception\InvalidJsonException;

class ApiResponse extends Response implements ArrayAccess
{
    // response assoc array
    protected $response;

    public function parseBody(): ApiResponse
    {
        if (!empty($this->body)) {
            $response = json_decode($this->body, true);

            if (!$response && JSON_ERROR_NONE !== ($error = json_last_error())) {
                throw new InvalidJsonException(
                    sprintf("Invalid JSON in the API response body: %s", $this->body),
                    $error
                );
            }

            $this->response = $response;
        }

        return $this;
    }

    /**
     * Allow to access for the property throw class method
     *
     * @param string $name      method name
     * @param mixed  $arguments method parameters
     *
     * @throws InvalidArgumentException
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        // convert getSomeProperty to someProperty
        $propertyName = strtolower(substr($name, 3, 1)) . substr($name, 4);

        if (!isset($this->response[$propertyName])) {
            throw new InvalidArgumentException("Method \"$name\" not found");
        }

        return $this->response[$propertyName];
    }

    /**
     * Allow to access for the property throw object property
     *
     * @param string $name property name
     *
     * @throws InvalidArgumentException
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (!isset($this->response[$name])) {
            throw new InvalidArgumentException("Property \"$name\" not found");
        }

        return $this->response[$name];
    }

    /**
     * Offset set
     *
     * @param mixed $offset offset
     * @param mixed $value  value
     *
     * @throws BadMethodCallException
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        throw new BadMethodCallException('This activity not allowed');
    }

    /**
     * Offset unset
     *
     * @param mixed $offset offset
     *
     * @throws BadMethodCallException
     * @return void
     */
    public function offsetUnset($offset)
    {
        throw new BadMethodCallException('This call not allowed');
    }

    /**
     * Check offset
     *
     * @param mixed $offset offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->response[$offset]);
    }

    /**
     * Get offset
     *
     * @param mixed $offset offset
     *
     * @throws InvalidArgumentException
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        if (!isset($this->response[$offset])) {
            throw new InvalidArgumentException("Property \"$offset\" not found");
        }

        return $this->response[$offset];
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}