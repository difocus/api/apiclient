<?php

namespace ShopExpress\ApiClient\Response;

use InvalidArgumentException;

/**
 * Class ResponseBuilder
 * Builder for Response, used by Client
 */
class ResponseBuilder
{
    /**
     * The response to be built.
     *
     * @var Response
     */
    protected $response;

    /**
     * ResponseBuilder constructor.
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param $headerLine
     * @return $this
     */
    public function addHeader($headerLine)
    {
        $parts = explode(':', $headerLine, 2);
        if (2 !== count($parts)) {
            throw new InvalidArgumentException(
                sprintf('"%s" is not a valid HTTP header line', $headerLine)
            );
        }
        $name = trim($parts[0]);
        $value = trim($parts[1]);
        if ($this->response->hasHeader($name)) {
            $this->response->withHeader($name, $value, true);
        } else {
            $this->response->withHeader($name, $value);
        }
        
        return $this;
    }

    /**
     * Set response status from a status string.
     *
     * @param string $statusLine Response status as a string.
     *
     * @return $this
     *
     * @throws InvalidArgumentException For invalid status line.
     */
    public function setStatus($statusLine)
    {
        $parts = explode(' ', $statusLine, 3);
        if (count($parts) < 2 || 0 !== strpos(strtolower($parts[0]), 'http/')) {
            throw new InvalidArgumentException(
                sprintf('"%s" is not a valid HTTP status line', $statusLine)
            );
        }
        $reasonPhrase = count($parts) > 2 ? $parts[2] : '';
        $this->response
            ->setStatusCode((int) $parts[1]);
        $this->response
            ->setReasonPhrase($reasonPhrase);
        
        return $this;
    }
}
