<?php
namespace ShopExpress\ApiClient\Test;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ShopExpress\ApiClient\ApiClient;
use ShopExpress\ApiClient\Exception\ApiException;
use ShopExpress\ApiClient\Exception\InvalidJsonException;
use ShopExpress\ApiClient\Exception\InvalidOperatorAndValueException;
use ShopExpress\ApiClient\Exception\NetworkException;
use ShopExpress\ApiClient\Exception\RequestException;
use ShopExpress\ApiClient\Request\RequestBuilder;
use ShopExpress\ApiClient\Request\RequestBuilderContentFilter;
use ShopExpress\ApiClient\Request\RequestBuilderFilter;
use ShopExpress\ApiClient\Request\RequestBuilderSort;
use ShopExpress\ApiClient\Response\ApiResponse;
use ShopExpress\ApiClient\Response\Response;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;

class ApiClientTest extends TestCase
{
	protected static $config;

	public static function setUpBeforeClass()
	{
        static::$config = [
            'apiToken' => getenv('TEST_SITE_API_TOKEN'),
            'apiUrl' => getenv('TEST_SITE_HOST') . '/api/',
        ];
	}

    /**
     * @param ApiClient $instance
     *
     * @throws NetworkException
     * @throws RequestException
     * @throws InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @return mixed
     */
	public function simpleQuery($instance)
	{
	    $requestBuilder = (new RequestBuilder("objects"))
            ->paginate(1, 0);

		return $instance->get($requestBuilder);
	}

    /**
     * @dataProvider invalidInitConfigProvider
     *
     * @param $apiKey
     * @param $apiUrl
     */
	public function testInvalidInit($apiKey, $apiUrl): void
    {
        $this->expectException(InvalidArgumentException::class);

    	new ApiClient($apiKey, $apiUrl);
    }

    /**
     * @return ApiClient
     */
    public function testValidInit(): ApiClient
    {
    	$instance = new ApiClient(static::$config['apiUrl'], static::$config['apiToken']);

    	self::assertTrue(true);

    	return $instance;
    }

    /**
     * @depends testValidInit
     *
     * @param $instance
     *
     * @throws InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     * @return ApiClient
     */
    public function testSendValidInitRequest($instance): ApiClient
    {
        $response = $this->simpleQuery($instance);
        self::assertInstanceOf(ApiResponse::class, $response);
        self::assertEquals(200, $response->getStatusCode());

        return $instance;
    }

    /**
     */
    public function testSendInvalidInitRequest1()
    {
        $this->expectException(ApiException::class);

        $instance = new ApiClient(static::$config['apiUrl'], "wrong");
    	$this->simpleQuery($instance);
    }

    /**
     */
    public function testSendInvalidInitRequest3()
    {
        $this->expectException(InvalidArgumentException::class);

        $instance = new ApiClient("example.ru", static::$config['apiToken']);
    	$this->simpleQuery($instance);
    }

    /**
     * @depends testValidInit
     * @param $instance
     */
    public function testGetApiKey(ApiClient $instance)
    {
    	self::assertEquals(static::$config['apiToken'], $instance->getApiToken());
    }

    /**
     * @depends testValidInit
     * @param $instance
     */
    public function testGetApiUrl(ApiClient $instance)
    {
    	self::assertEquals(static::$config['apiUrl'], $instance->getApiUrl());
    }

    /**
     * @depends testValidInit
     *
     * @param ApiClient $instance
     *
     * @throws InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     */
    public function testGetConstants(ApiClient $instance)
    {
        $requestBuilder = (new RequestBuilder('constants'))
            ->addFilter(new RequestBuilderFilter('name', '=', 'site_host'));
        
        $response = $instance->get($requestBuilder);

        self::assertEquals(true, $response->result['success']);
        self::assertCount(1, $response->result['data']);
        self::assertEquals('site_host', $response->result['data'][0]['name']);
    }

    /**
     * @depends testValidInit
     *
     * @param ApiClient $instance
     *
     * @throws InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     */
    public function testGetConstantsWithSort(ApiClient $instance)
    {
        $requestBuilder = (new RequestBuilder('constants'))
            ->addFilter(new RequestBuilderFilter('name', 'like', 'site_%'))
            ->setSort(new RequestBuilderSort('id', RequestBuilderSort::DESC));

        $response = $instance->get($requestBuilder);

        self::assertGreaterThan($response->result['data'][1]['id'], $response->result['data'][0]['id']);
    }

    /**
     * @depends testValidInit
     */
    public function testCreateConstant(ApiClient $instance)
    {
        $requestBuilder = (new RequestBuilder('constants'))
            ->setData(['constants' => [
                'name' => 'new_constant',
                'value' => $value = '12345',
            ]]);

        $response = $instance->create($requestBuilder);

        self::assertEquals(true, $response->result['success']);

        $constant = $response->result['data'];

        self::assertEquals($value, $constant['value']);

        return compact('instance', 'constant');
    }

    /**
     * @depends testCreateConstant
     */
    public function testUpdateConstant($data)
    {
        $instance = $data['instance'];

        $requestBuilder = (new RequestBuilder('constants'))
            ->setData(['constants' => [
                'id' => $data['constant']['id'],
                'value' => $value = '11111',
            ]]);

        $response = $data['instance']->update($requestBuilder);

        self::assertEquals(true, $response->result['success']);

        $constant = $response->result['data'][0];

        self::assertEquals($value, $constant['value']);

        return compact('instance', 'constant');
    }

    /**
     * @depends testUpdateConstant
     */
    public function testDeleteConstant($data)
    {
        $instance = $data['instance'];

        $requestBuilder = (new RequestBuilder('constants', $data['constant']['id']));

        $response = $data['instance']->delete($requestBuilder);

        self::assertEquals(true, $response->result['success']);
    }

    /**
     * @throws InvalidOperatorAndValueException
     * @throws InvalidRequestException
     * @throws RequestException
     */
    public function testGetByObjectContent(): void
    {
        $requestBuilder = (new RequestBuilder('objects'))
            ->addFilter(new RequestBuilderContentFilter('name', '<>', 'admin'))
            ->addFilter(new RequestBuilderContentFilter('gender', '=', 'male'))
            ->addFilter(new RequestBuilderFilter('type', '=', 2));

        $request = $requestBuilder->build(RequestBuilder::REQUEST_TYPE_GET);

        self::assertEquals(['filters' => [
            'content' => [
                'name' => ['<>', 'admin'],
                'gender' => ['=', 'male'],
            ],
            'type' => ['=', 2],
        ], 'sort' => [], 'include' => []], $request->get());
    }

    public function invalidInitConfigProvider()
    {
    	return [
    		["", "s"],
    		["s", "s"],
    		["s", ""],
    		["", "s"],
    		["s", ""],
    		["", ""],
    		["", "example.ru"],
    	];
    }

    public static function tearDownAfterClass()
    {
    	static::$config = null;
    }
}
