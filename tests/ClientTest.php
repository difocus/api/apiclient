<?php

namespace ShopExpress\ApiClient\Test;

use PHPUnit\Framework\TestCase;
use ShopExpress\ApiClient\Client;
use ShopExpress\ApiClient\Exception\NetworkException;
use ShopExpress\ApiClient\Exception\RequestException;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use ShopExpress\RequestResponse\Request\Request;
use ShopExpress\ApiClient\Response\Response;

class ClientTest extends TestCase
{
    protected static $config;

    /**
     * @throws NetworkException
     * @throws RequestException
     * @throws InvalidRequestException
     */
    public function testSendTooLargeRequest(): void
    {
        $payload = [];
        for ($i = 0; $i < 250000; $i++) {
            $payload['key' . $i] = $i;
        }

        $client = new Client([
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 600,
        ]);

        $response = $client->sendRequest(new Request(
            'http://127.0.0.1/index1.php/',
            json_encode($payload),
            [], [], [], [], [], 'POST', []
        ));

        self::assertEquals(200, $response->getStatusCode());
        self::assertFalse(empty($response->getBody()));

        $responseArr = @json_decode($response->getBody(), true);
        self::assertFalse(!$response && JSON_ERROR_NONE !== json_last_error());
        self::assertTrue($responseArr['success']);
    }
}