<?php

$requestBody = file_get_contents('php://input');

try {
    if ($requestBody === false) {
        throw new Exception('Can\'t read request body');
    }

    if (empty($requestBody)) {
        throw new Exception('Request body is empty');
    }


    $decompressedBody = gzdecode($requestBody);
    if ($decompressedBody === false) {
        throw new Exception('Can\'t decode request body');
    }

    $bodyArr = json_decode($decompressedBody, true);

    if (!$bodyArr  && JSON_ERROR_NONE !== ($error = json_last_error())) {
        throw new Exception('Can\'t decode request body');
    }

    if ($bodyArr['key9'] ?? null === 9) {
        echo json_encode(['success' => true]);
        exit();
    }

    throw new Exception('Unexpected request body content');
} catch (Throwable $e) {
    header('HTTP/1.1 500 Internal Server Error');
    echo json_encode([
        'success' => false,
        'message' => $e->getMessage()
    ]);
    exit();
}
